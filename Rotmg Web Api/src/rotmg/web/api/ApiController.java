/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rotmg.web.api;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


public class ApiController 
{
    public static String buildParams(List<NameValuePair> params)
    {
        return URLEncodedUtils.format(params, "utf-8");
    }

    public static String sendHttpGetRequest(String targetUrl)
    {
        try
        {
            HttpGet httpGet = new HttpGet(targetUrl);
            CloseableHttpClient client = HttpClients.createDefault();
            HttpResponse response = client.execute(httpGet);
            HttpEntity httpEntity = response.getEntity();
            String httpResponseString = EntityUtils.toString(httpEntity);
            return httpResponseString;
        }
        catch (IOException ex)
        {
            Logger.getLogger(RotmgWebApi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String getFinalUrl(String apiUrl, List<NameValuePair> params)
    {
        String finalUrl = RotmgWebApi.urlBase + apiUrl;
        if (params != null)
        {
            finalUrl += buildParams(params);
        }
        return finalUrl;
    }
}

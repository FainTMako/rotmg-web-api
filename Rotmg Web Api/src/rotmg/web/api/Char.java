/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rotmg.web.api;

import java.util.List;
import org.apache.http.NameValuePair;


public class Char extends ApiController
{
    public static String apiBaseUrl = "char/";
    
    
    public static String listUrl(List<NameValuePair> params)
    {
        String apiUrl = apiBaseUrl + "list?";
        String finalUrl = getFinalUrl(apiUrl, params);
        return finalUrl;
    }
    public static String list(List<NameValuePair> params)
    {
        
        return sendHttpGetRequest(listUrl(params));
    }
}
